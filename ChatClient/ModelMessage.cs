﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace ChatClient
{
    public class ModelMessage
    {
        public string UserName { get; set; }
        public string Message { get; set; }
        public DateTime DateTimeMessage { get; set; }
        [JsonIgnore]
        public bool HiddenProperty { get; set; }
    }
}

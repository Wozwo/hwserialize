﻿using System.Configuration;
using System;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Text.Json;


namespace ChatClient
{
    class Program
    {
        static string userName;
        
        private static string host = ConfigurationManager.AppSettings["host"];
        private static int port = int.Parse(ConfigurationManager.AppSettings["port"]);
        static TcpClient client;
        static NetworkStream stream;

        static void Main(string[] args)
        {
            Console.Write("Введите свое имя: ");
            userName = Console.ReadLine();
            client = new TcpClient();
            try
            {
                client.Connect(host, port); //подключение клиента
                stream = client.GetStream(); // получаем поток

                var json = JsonSerializer.Serialize<ModelMessage>(new ModelMessage { 
                    UserName = userName, 
                    Message = "", 
                    DateTimeMessage = DateTime.Now });
                byte[] data = Encoding.Unicode.GetBytes(json);
                stream.Write(data, 0, data.Length);

                // запускаем новый поток для получения данных
                Thread receiveThread = new Thread(new ThreadStart(ReceiveMessage));
                receiveThread.Start(); //старт потока
                Console.WriteLine("Добро пожаловать, {0}", userName);
                SendMessage();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Disconnect();
            }
        }
        // отправка сообщений
        static void SendMessage()
        {
            Console.WriteLine("Введите сообщение: ");
            
            while (true)
            {
               
                string message = Console.ReadLine();
                var modelMessage = new ModelMessage { 
                    UserName = userName, 
                    Message = message, 
                    DateTimeMessage = DateTime.Now };

                string json = JsonSerializer.Serialize<ModelMessage>(modelMessage);

                byte[] data = Encoding.Unicode.GetBytes(json);
                stream.Write(data, 0, data.Length);
            }
        }
        // получение сообщений
        static void ReceiveMessage()
        {
            while (true)
            {
                try
                {
                    byte[] data = new byte[1024]; // буфер для получаемых данных
                    int bytes = 0;
                    do
                    {
                        bytes = stream.Read(data, 0, data.Length);
                        var json = Encoding.Unicode.GetString(data, 0, bytes);
                        var messageModel = JsonSerializer.Deserialize<ModelMessage>(json);
                        WriteMessage(messageModel);
                    }
                    while (stream.DataAvailable);
                    //вывод сообщения
                }
                catch
                {
                    Console.WriteLine("Подключение прервано!"); //соединение было прервано
                    Console.ReadLine();
                    Disconnect();
                }
            }
        }

        private static void WriteMessage(ModelMessage modelMessage)
        {
            var timemessage = $"{modelMessage.DateTimeMessage.Hour}:{modelMessage.DateTimeMessage.Minute}:{modelMessage.DateTimeMessage.Second} ";
            var message = $"{timemessage} {modelMessage.UserName}: {modelMessage.Message}";
            Console.WriteLine(message);
        }

        static void Disconnect()
        {
            if (stream != null)
                stream.Close();//отключение потока
            if (client != null)
                client.Close();//отключение клиента
            Environment.Exit(0); //завершение процесса
        }
    }
}

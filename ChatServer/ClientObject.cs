﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Text.Json;

namespace ChatServer
{
    public class ClientObject
    {
        protected internal string Id { get; private set; }
        protected internal NetworkStream Stream { get; private set; }
        string userName;
        TcpClient client;
        ServerObject server; // объект сервера

        public ClientObject(TcpClient tcpClient, ServerObject serverObject)
        {
            Id = Guid.NewGuid().ToString();
            client = tcpClient;
            server = serverObject;
            serverObject.AddConnection(this);
        }

        public void Process()
        {
            try
            {
                Stream = client.GetStream();
                var modelMessage = ProcessingCustomer(GetModelMessage());
                // посылаем сообщение о входе в чат всем подключенным пользователям
                server.BroadcastMessage(modelMessage, this.Id);
                WriteMessage(modelMessage);
                // в бесконечном цикле получаем сообщения от клиента
                while (true)
                {
                    try
                    {
                        modelMessage = GetModelMessage();
                        WriteMessage(modelMessage);
                        server.BroadcastMessage(modelMessage, this.Id);
                    }
                    catch
                    {
                        modelMessage = new ModelMessage { 
                            UserName = userName, 
                            Message = $"покинул чат", 
                            DateTimeMessage = DateTime.Now };
                        WriteMessage(modelMessage);
                        server.BroadcastMessage(modelMessage, this.Id);
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                server.BroadcastMessage(new ModelMessage { 
                    Message = e.Message, 
                    DateTimeMessage = DateTime.Now }, this.Id);
            }
            finally
            {
                // в случае выхода из цикла закрываем ресурсы
                server.RemoveConnection(this.Id);
                Close();
            }
        }

        private ModelMessage ProcessingCustomer(ModelMessage modelMessage)
        {
            userName = modelMessage.UserName;
            var model = new ModelMessage { 
                UserName = userName, 
                Message = $"вошел в чат", 
                DateTimeMessage = modelMessage.DateTimeMessage };
            return model;
        }

        private void WriteMessage(ModelMessage modelMessage)
        {
            var timemessage = $"{modelMessage.DateTimeMessage.Hour}:{modelMessage.DateTimeMessage.Minute}:{modelMessage.DateTimeMessage.Second} ";
            var message = $"{timemessage} {userName}: {modelMessage.Message}";
            Console.WriteLine(message);
        }

        // чтение входящего сообщения и преобразование в строку
        private ModelMessage GetModelMessage()
        {

            byte[] data = new byte[1024]; // буфер для получаемых данных
            ModelMessage messageModel = null;
            int bytes = 0;
            do
            {
                bytes = Stream.Read(data, 0, data.Length);
                messageModel = JsonSerializer.Deserialize<ModelMessage>(Encoding.Unicode.GetString(data, 0, bytes));
            }
            while (Stream.DataAvailable);
            return messageModel;
        }

        // закрытие подключения
        protected internal void Close()
        {
            if (Stream != null)
                Stream.Close();
            if (client != null)
                client.Close();
        }
    }
}
